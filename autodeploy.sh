#!/bin/bash

# Stop on the first sign of trouble
set -e

# The name of your script AND your make target
TARGET="main"

# The directory where your Dockerfile and source code are located
PROJECT_DIR="./cross_compilation"

# The IP address or hostname of your EV3 Brick
IP="192.168.85.191"
USERNAME="robot"
EV3_PASS="maker"

EV3_HOST="$USERNAME@$IP"

ping -w 1 -c 1 $IP || exit 1

# Build the project using Docker
echo "Building the project..."
cd "${PROJECT_DIR}"
docker compose up --build
cd ..

# The build directory
BUILD_DIR="build"

# Copy the binary to the EV3 Brick's home directory
echo "Transferring the binary to the EV3 Brick..."
sshpass -p "${EV3_PASS}" scp "${BUILD_DIR}/${TARGET}" "${EV3_HOST}:~/"

# Run the program (optional)
# Uncomment the following lines if you want to run the program after transferring
#echo "Running the program on the EV3 Brick..."
#sshpass -p "${EV3_PASS}" ssh "${EV3_HOST}" "./${TARGET}"

echo "Deployment complete"