# OS Project

[Project page - perso.telecom-paristech.fr/apvrille](https://perso.telecom-paristech.fr/apvrille/OS/projects_fall2023.html)

[Project website - 3f-robot.nolliv22.com](https://3f-robot.nolliv22.com/)

## Team members
- **Moaad MAAROUFI** (Captain)
- **Villon CHEN**
- **Meriem LAMRANI**

- **Ludovic Aprville** (Supervisor)

## Installation

Guide:
- [Flash ev3dev image - ev3dev.org](https://www.ev3dev.org/docs/getting-started/)
- [Connect via USB - ev3dev.org](https://www.ev3dev.org/docs/tutorials/connecting-to-the-internet-via-usb/)
  
Useful information:
- the IP address is written on the ev3dev or use the hostname: `ev3dev.local`
- User (root): `robot`, Password: `maker`

Once the previous steps are done (ev3dev flashed and access to an Internet connexion), follow these steps (from the project page):

1. Change the sources for apt as they are outdated:
```bash
$ sudo apt edit-sources

# Overwrite all with those lines:
deb http://archive.debian.org/debian/ stretch main contrib non-free
deb http://archive.debian.org/debian-security stretch/updates main contrib non-free

deb http://archive.ev3dev.org/debian stretch main
deb-src http://archive.ev3dev.org/debian stretch main
```

2. Update and upgrade (very very very slow)
```console
$ sudo apt-get update && sudo apt-get upgrade
```

3. Install build tools
```console
$ sudo apt-get install gcc make build-essential git
```

4. Install [LEGO MINDSTORMS EV3 Debian C library](https://github.com/in4lio/ev3dev-c)
```console
$ cd /home/robot
$ git clone https://github.com/in4lio/ev3dev-c.git
$ cd ev3dev-c
$ git submodule update --init --recursive
$ cd source/ev3/
$ make
$ sudo make install
$ make shared
$ sudo make shared-install
```