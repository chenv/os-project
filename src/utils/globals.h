#ifndef GLOBALS_H
#define GLOBALS_H

#include <stdint.h>

extern int DEBUG;

extern uint8_t sn_sonar;
extern uint8_t sn_gyro;
extern uint8_t sn_right;
extern uint8_t sn_left;
extern uint8_t sn_grab;
extern uint8_t sn_color;

extern int angle_offset;

#endif // GLOBALS_H
