#include "common.h"
#include "globals.h"



// Rotate from initial angle (Robot's "North")
int rotate(int target_angle){
    int dt = 100; 
    int base_speed = 250;
    int left_speed;
    int rigth_speed;

    int stuck_counter = 0;
    int stuck_angle = get_angle();

    // Increase the angle as the rotation is not perfect (we noticed that it doesn't turn enough)
    if (target_angle > 0){
        target_angle -= 1;
    } else if (target_angle < 0){
        target_angle += 1;
    }
    
    while (1){
        int speed = base_speed;
        int current_angle = get_angle();
        int diff = target_angle - current_angle;

        // Prevent being stuck due to low rotation speed
        if (current_angle == stuck_angle){
            // Increase the roation speed if stuck
            if (stuck_counter == 10){
                stuck_counter = 0;
                speed *= 4;
            } else {
                stuck_counter++;
            }
        } else {
            stuck_angle = current_angle;
            stuck_counter = 0;
        }

        // Handle the special case (rotation(180))
        // Since the angle can vary between 179 180 -179 -178, we have to make sure the angle difference is positive otherwise it will make an entire turn
        if (abs(target_angle) == 180 && current_angle < 0){
            diff = -(current_angle + 180) ;
        }

        // Slow the speed when near the target to avoid oscillating
        if (abs(diff) < 15){
            speed = speed / 4;
        } else if (abs(diff) < 5){
            speed = speed / 8;
        }

        // Positive speed on one wheel and negative speed on the other to rotate
        if (diff > 0){
            left_speed = speed;
            rigth_speed = -speed;
        } else if (diff < 0){
            left_speed = -speed;
            rigth_speed = speed;
        // Stop when right angle
        } else {
            set_tacho_stop_action_inx(sn_left, TACHO_HOLD);
            set_tacho_stop_action_inx(sn_right, TACHO_HOLD);
            set_tacho_command_inx(sn_left, TACHO_STOP);
            set_tacho_command_inx(sn_right, TACHO_STOP);
            break;
        }
        
        // Actionate the motors for a small time (so that we can adapt in the next iteration)
        set_tacho_time_sp(sn_left, 1*dt);
        set_tacho_time_sp(sn_right, 1*dt);
        set_tacho_speed_sp(sn_left, left_speed);
        set_tacho_speed_sp(sn_right, rigth_speed);
        set_tacho_command_inx(sn_left, TACHO_RUN_TIMED);
        set_tacho_command_inx(sn_right, TACHO_RUN_TIMED);

        //Debug
        if (DEBUG){
            printf("Debugging function: rotate\n");
            printf("Target: %d, Current: %d\n", target_angle, current_angle);
            printf("Diff: %d\n", diff);
            printf("Speed: %d | %d\n", left_speed, rigth_speed);
            printf("Distance: %d\n", get_distance());
        }

        Sleep(dt);
    }

    return target_angle;
}


void go_until_distance(int base_speed, int duration, int target_distance){
    int dt = 100;
    int left_speed;
    int rigth_speed;
    int initial_angle = get_angle();

    while (duration > 0){
        int speed = base_speed;
        int current_distance = get_distance();
        int current_angle = get_angle();

        // Slower near the target
        if (abs(target_distance - current_distance) < abs(target_distance / 10)){
            speed = speed / 2;
        }

        // Keep aligned
        if (current_angle > initial_angle){
            left_speed = 1.05 * -speed;
            rigth_speed = 1 * -speed;
        } else if (current_angle < initial_angle){
            left_speed = 1 * -speed;
            rigth_speed = 1.05 * -speed;
        } else {
            left_speed = -speed;
            rigth_speed = -speed;
        }

        // Stop when right distance (or smaller)
        if (current_distance < target_distance){
            set_tacho_stop_action_inx(sn_left, TACHO_HOLD);
            set_tacho_stop_action_inx(sn_right, TACHO_HOLD);
            set_tacho_command_inx(sn_left, TACHO_STOP);
            set_tacho_command_inx(sn_right, TACHO_STOP);
            break;
        }
        
        // Actionate the motors for a small time (so that we can adapt in the next iteration)
        set_tacho_time_sp(sn_left, 2*dt);
        set_tacho_time_sp(sn_right, 2*dt);
        set_tacho_speed_sp(sn_left, left_speed);
        set_tacho_speed_sp(sn_right, rigth_speed);
        set_tacho_command_inx(sn_left, TACHO_RUN_TIMED);
        set_tacho_command_inx(sn_right, TACHO_RUN_TIMED);

        //Debug
        if (DEBUG){
            printf("Debugging function: go_until_distance\n");
            printf("Current: %d | Target: %d\n", current_distance, target_distance);
            printf("Speed: %d | %d\n", left_speed, rigth_speed);
            printf("Angle: %d\n", current_angle);
        }

        duration -= dt;
        Sleep(dt);
    }
}

void go_until_color(int base_speed, int duration, int target_color, int min_distance){
    int dt = 10;

    while (duration > 0){
        int speed = base_speed;
        int current_angle = get_angle();

        // Stop if hit the distance limit
        int current_distance = get_distance();
        if (current_distance < min_distance){
            set_tacho_stop_action_inx(sn_left, TACHO_HOLD);
            set_tacho_stop_action_inx(sn_right, TACHO_HOLD);
            set_tacho_command_inx(sn_left, TACHO_STOP);
            set_tacho_command_inx(sn_right, TACHO_STOP);
            break;
        }

        // Stop if encounter the color
        int current_color = get_color();
        if (current_color == target_color){
            set_tacho_stop_action_inx(sn_left, TACHO_HOLD);
            set_tacho_stop_action_inx(sn_right, TACHO_HOLD);
            set_tacho_command_inx(sn_left, TACHO_STOP);
            set_tacho_command_inx(sn_right, TACHO_STOP);
            break;
        }
        
        // Actionate the motors for a small time (so that we can adapt in the next iteration)
        set_tacho_time_sp(sn_left, 100);
        set_tacho_time_sp(sn_right, 100);
        set_tacho_speed_sp(sn_left, -speed);
        set_tacho_speed_sp(sn_right, -speed);
        set_tacho_command_inx(sn_left, TACHO_RUN_TIMED);
        set_tacho_command_inx(sn_right, TACHO_RUN_TIMED);

        //Debug
        if (DEBUG){
            printf("Debugging function: go_until_color\n");
            printf("Angle: %d\n", current_angle);
            printf("Color: %d\n", current_color);
        }

        duration -= dt;
        Sleep(dt);
    }
}

void centering(){
    int ARENA_MIN_WIDTH = 800;
    int FORWARD_SPEED = 200;
    int DT = 250;
    int left;
    int right;

    // Computing left side and right side distance until it's valid (to avoid centering with respect to an obstance which will break the centering)
    while (1){
        rotate(90);
        left = get_distance();
        Sleep(250);
        rotate(-90);
        right = get_distance();

        // If the left side plus the right side is higher than the threshold we determined, then it means that there is no obstacle and we can center 
        if (left + right > ARENA_MIN_WIDTH){
            break;
        } else {
            rotate(0);
            go_until_distance(FORWARD_SPEED, DT, 150);
        }
    }

    // Rotate to the longest side
    if (left > right){
        rotate(90);
    }

    // Go forward until the middle to center
    go_until_distance(200, INT32_MAX, (left + right)/2);
    Sleep(250);
    rotate(0);
}