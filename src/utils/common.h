#ifndef COMMON_H
#define COMMON_H

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <time.h>

#include "ev3.h"
#include "ev3_port.h"
#include "ev3_tacho.h"
#include "ev3_sensor.h"


#include "../sensors/gyro.h"
#include "../sensors/color.h"
#include "../sensors/sonar.h"
#include "../motors/wheels.h"
#include "../motors/grabber.h"


#define Sleep( msec ) usleep(( msec ) * 1000 )

// Function prototypes
int rotate(int target_angle);
void go_until_distance(int base_speed, int duration, int target_distance);
void go_until_color(int base_speed, int duration, int target_color, int min_distance);
void centering();


#endif // COMMON_H


