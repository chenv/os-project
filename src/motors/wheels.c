#include "../utils/globals.h"
#include "../utils/common.h"


// Go for a specific duration and keep the right angle
void go_for(int base_speed, int duration){
    int dt = 100;
    int left_speed;
    int rigth_speed;
    int initial_angle = get_angle();

    while (duration > 0){
        int speed = base_speed;
        int current_angle = get_angle();

        // Keep aligned
        if (current_angle > initial_angle){
            left_speed = 1.05 * -speed;
            rigth_speed = 1 * -speed;
        } else if (current_angle < initial_angle){
            left_speed = 1 * -speed;
            rigth_speed = 1.05 * -speed;
        } else {
            left_speed = -speed;
            rigth_speed = -speed;
        }

        // Actionate the motors for a small time (so that we can adapt in the next iteration)
        set_tacho_time_sp(sn_left, 2*dt);
        set_tacho_time_sp(sn_right, 2*dt);
        set_tacho_speed_sp(sn_left, left_speed);
        set_tacho_speed_sp(sn_right, rigth_speed);
        set_tacho_command_inx(sn_left, TACHO_RUN_TIMED);
        set_tacho_command_inx(sn_right, TACHO_RUN_TIMED);

        //Debug
        if (DEBUG){
            printf("Debugging function: go_for\n");
            printf("Speed: %d | %d\n", left_speed, rigth_speed);
            printf("Angle: %d\n", current_angle);
        }

        duration -= dt;
        Sleep(dt);
    }
}