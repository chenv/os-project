#include "../utils/globals.h"
#include "../utils/common.h"


// Rotate the grabber's motor to open or close the grabbing mecanism
void grab(int angle){
    FLAGS_T state;  // Declaration of the state variable    
    set_tacho_command_inx(sn_grab, TACHO_RESET); // Reset the motor encoder

    // Grab action
    set_tacho_stop_action_inx(sn_grab, TACHO_HOLD);
    set_tacho_speed_sp(sn_grab, 800); // Set speed for a controlled grab
    set_tacho_position_sp(sn_grab, angle); // Set to the position you want to grab
    set_tacho_command_inx(sn_grab, TACHO_RUN_TO_REL_POS);

    // Wait for the motor to finish the grabbing action
    while (get_tacho_state_flags(sn_grab, &state), (state & TACHO_RUNNING)) {
        Sleep(10);
    }
}

// Grab flag when in range of the flag
void grab_flag(){
    int FLAG_GRAB_DISTANCE = 70;
    
    // Get close to the flag
    go_until_distance(200, INT32_MAX, FLAG_GRAB_DISTANCE);
    go_for(100, 900);
    
    // Grab without lifting the flag* (* to avoid when we lift the flag, the flag will hit the wall and be badly inclined)
    grab(1700);
    // Go forward
    go_for(-100, 800);
    // Lift the flag
    grab(700);
}

// Release the flag when in range of the goal
void release_flag(){
    int FLAG_RELEASE_DISTANCE = 150;

    // Get close to the goal
    go_until_distance(200, INT32_MAX, FLAG_RELEASE_DISTANCE);

    // Put the flag on the ground (not releasing completely)
    grab(-900);
    // Go forward to push the flag toward the goal
    go_for(100, 1500);
    // Completely release it
    grab(-1500);
}