#ifndef GRABBER_H
#define GRABBER_H

void grab(int angle);
void grab_flag();
void release_flag();

#endif // GRABBER_H
