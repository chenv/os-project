#include "utils/globals.h"
#include "utils/common.h"

int DEBUG = 1;

uint8_t sn_sonar;
uint8_t sn_gyro;
uint8_t sn_right;
uint8_t sn_left;
uint8_t sn_grab;
uint8_t sn_color;

int angle_offset = 0;


int main(void){
    // Initialize the random
    srand(time(NULL));

    ev3_sensor_init();

    // Connect to sensors
    if (!ev3_search_sensor(LEGO_EV3_US, &sn_sonar, 0)){
        printf("Sonar not connected!\n");
        return -1;
    }

    if (!ev3_search_sensor(LEGO_EV3_GYRO, &sn_gyro ,0)){
        printf("Gyroscope not connected!\n");
        return -1;
    }

    if (!ev3_search_sensor( LEGO_EV3_COLOR, &sn_color, 0)) {
        printf("Color sensor not connected!\n");
    }

    // Change color mode
    set_sensor_mode(sn_color, "COL-COLOR");

    // Calibrate the Gyroscope
    // By default, 0 is the "North" of the robot
    set_sensor_mode(sn_gyro, "GYRO-CAL"); // Calibrate mode: set the angle to 0 (doesn't compute angle and always returns 0)
    set_sensor_mode(sn_gyro, "GYRO-ANG"); // Angle mode: compute the angle

    // Initialize motors and check if there are connected
    while (ev3_tacho_init() < 1) Sleep(1000);

    if (!ev3_search_tacho_plugged_in(OUTPUT_B, 0, &sn_left, 0)){
        printf("Left wheel not connected!\n");
        return -1;
    }

    if (!ev3_search_tacho_plugged_in(OUTPUT_D, 0, &sn_right, 0)){
        printf("Right wheel not connected!\n");
        return -1;
    }

    if (!ev3_search_tacho_plugged_in(OUTPUT_A, 0, &sn_grab, 0)){
        printf("Grabber not connected!\n");
        return -1;
    }

    DEBUG = 1;

    // Green line color value
    int LINE_COLOR = 4;

    // Trip parameters (!!! Higher speed can cause the color sensor to not find the green line)
    int MAX_DISTANCE = 150;
    int DT = 250;
    int SPEED = 350;

    // Store the previous rotation made to know which side of the arena the robot is actually in
    // PREVIOUS_ROTATION < 0 <=> right side (the robot has turn -90 before)
    // PREVIOUS_ROTATION > 0 <=> left side (the robot has turn 90 before)
    int PREVIOUS_ROTATION;

    // Random initial direction
    int INITIAL_DIRECTION;
    if (rand() % 2){
        INITIAL_DIRECTION = 30;
    } else {
        INITIAL_DIRECTION = -30;
    }

    // For loop (2 iterations) pour l'aller et le retour
    for (int i=0; i < 2; i++){
        PREVIOUS_ROTATION = rotate(INITIAL_DIRECTION);
        go_until_distance(SPEED, 3200, MAX_DISTANCE);
        rotate(0);

        // While not detecting the green line then keep going forward and avoid obstacles
        while (1){
            int distance = get_distance();
            int angle = get_angle();
            int color = get_color();

            // Cneter obstable detected: go backward and switch side
            if (distance == -1){
                // Go back
                rotate(0);                
                go_for(-SPEED, 500);

                // Switch side
                if (PREVIOUS_ROTATION < 0){
                    PREVIOUS_ROTATION = rotate(90);
                } else if (PREVIOUS_ROTATION > 0){
                    PREVIOUS_ROTATION = rotate(-90);
                }

            // Obstacle or wall detected (if the current distance is smaller than the threshold): go backward and switch side
            } else if (distance < MAX_DISTANCE){
                // If the robot is aligned with its "North" then go backward and switch side
                if (abs(angle) < 5){
                    go_for(-SPEED, 500);
                    if (PREVIOUS_ROTATION < 0){
                        PREVIOUS_ROTATION = rotate(90);
                    } else if (PREVIOUS_ROTATION > 0){
                        PREVIOUS_ROTATION = rotate(-90);
                    }
                // If the robot is not aligned with its "North", align back
                } else if (abs(angle) - 90 < 5){
                    rotate(0);
                }
            // Break if the green line is detected
            } else if (color == 4){
                break;
            // No obstacle, keep going forward for a small time DT (to check periodically the distance and the color)
            } else {
                go_until_color(SPEED, DT, LINE_COLOR, MAX_DISTANCE);
            }
        }

        // Aller seulement
        if (i == 0){
            // Centering as we know the flag is in the middle
            centering();
            // Grab the flag
            grab_flag();
            // Change the robot's "North" to (180) (U turn to go back to the starting point)
            angle_offset = 180-2;
        // Retour seulement
        } else {
            // Release the flag
            release_flag();
            // Go forward to leave the flag alone
            go_for(-200, 2000);
        }
    }

    ev3_uninit();
    return 0;
}
