#include "../utils/globals.h"
#include "../utils/common.h"

// Get dista,ce
int get_distance(){
    float input;
    get_sensor_value0(sn_sonar, &input);
    int value = (int) input;
    
    // If obstable detected (value equals 2550 but since we know that the arena is not larger than 2500, we will keep this value for safety)
    if (value > 2500){
        value = -1; // -1 is used by other function to know that we hit the obstacle
    }
    return value;
}