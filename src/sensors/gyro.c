#include "../utils/globals.h"
#include "../utils/common.h"

// Get angle
int get_angle(){
    // Return angle in [-179; 180]
    float input;
    get_sensor_value0(sn_gyro, &input);
    int value = (int) input + angle_offset; // Resetting the gyro to define the new "North" didn't work so we use an offset instead

    // Modulo operation
    while (value > 180){
        value -= 360;
    }
    while (value < -179){
        value += 360;
    }

    // Anticlockwise direction
    return -value;
}