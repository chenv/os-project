#include "../utils/globals.h"
#include "../utils/common.h"

// Get color value (color_value in [0, 6] (COL-COLOR))
int get_color(){
    int value;
    get_sensor_value(0, sn_color, &value);
    return value;
}