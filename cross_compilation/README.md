# Cross compilation

*Based on [OS Project Fall 2023 - Section Cross compilation](https://perso.telecom-paristech.fr/apvrille/OS/projects_fall2023.html)*

Compile your source code on your machine and then deploy it on the ev3 in one command.

## Prerequisite

- Install Docker (https://docs.docker.com/engine/install/)
- Install Docker Compose OR the Compose plugin (https://docs.docker.com/compose/install/)
- Have folders `src` (for source code) and `build` (build output) in your project folder shown as followed:
```
./
  ├── build
  │   └── ...
  ├── src
  |   ├── ...
  │   └── Makefile
  ├── cross_compilation
  |   ├── docker-compose.yml
  |   ├── Dockerfile
  │   └── ...
  └── ...
```

## Description

The folders `src` and `build` will be mounted to the docker container to cross compile the code for ev3.

A `Makefile` must be present in the `src` directory as the container will only run `make` in the `src` folder.

[!!!] Make sure that:
  - the output directory is `build` in the `Makefile`;
  - the compiler is `arm-linux-gnueabi-gcc`;
  - ev3 sources are included with `-I/ev3dev-c/source/ev3`

Example of variables that could be used in the `Makefile`:
```Makefile
CC = arm-linux-gnueabi-gcc
EV3DEV_C = /ev3dev-c/source/ev3
BUILD_DIR = /tmp/build
```

Feel free to edit the [`Dockerfile`](Dockerfile) to change the command ran in the container.

## Usage 

To cross compile, run this command: 
```shell
TARGET=YOUR_MAKE_TARGET docker compose up --build # if using Compose plugin
TARGET=YOUR_MAKE_TARGET docker-compose up --build # if using Docker Compose
```

It may take some time if the image has never been built or there are modifications made to the `Dockerfile`.

The `--build` option ensures that the image has been built with the latest `Dockerfile`.

## Example

```console
./cross_compilation$ TARGET=tester docker compose up --build
[+] Building 0.0s (8/8) FINISHED                                               docker:default
 => [cross_compilation internal] load build definition from Dockerfile                   0.0s
 => => transferring dockerfile: 560B                                                     0.0s
 => [cross_compilation internal] load .dockerignore                                      0.0s
 => => transferring context: 2B                                                          0.0s
 => [cross_compilation internal] load metadata for docker.io/ev3dev/debian-stretch-cros  0.0s
 => [cross_compilation 1/5] FROM docker.io/ev3dev/debian-stretch-cross                   0.0s
 => CACHED [cross_compilation 2/5] RUN echo "deb http://archive.debian.org/debian stret  0.0s
 => CACHED [cross_compilation 3/5] RUN git clone https://github.com/in4lio/ev3dev-c.git  0.0s
 => CACHED [cross_compilation 4/5] WORKDIR /tmp/src                                      0.0s
 => [cross_compilation] exporting to image                                               0.0s
 => => exporting layers                                                                  0.0s
 => => writing image sha256:bb0d235cfd550ca46af669b64dac6c3734f67308edd3a51b6f04db36b1b  0.0s
 => => naming to docker.io/library/ev3dev_cross_compilation                              0.0s
[+] Running 1/0
 ✔ Container ev3dev_cross_compilation  Created                                           0.0s 
Attaching to ev3dev_cross_compilation
ev3dev_cross_compilation  | arm-linux-gnueabi-gcc -I/ev3dev-c/source/ev3 -O2 -std=gnu99 -W -Wall -Wno-comment -c tester.c -o /tmp/build/tester.o
ev3dev_cross_compilation  | arm-linux-gnueabi-gcc /tmp/build/tester.o -Wall -lm -lev3dev-c -o /tmp/build/tester
ev3dev_cross_compilation exited with code 0
./cross_compilation$ ls -l ../build/
total 40
-rwxr-xr-x 1 root root  8852 Dec 21 11:08 i2c
-rw-r--r-- 1 root root  2660 Dec 21 11:08 i2c.o
-rwxr-xr-x 1 root root 14264 Dec 21 11:14 tester
-rw-r--r-- 1 root root  6364 Dec 21 11:14 tester.o
```